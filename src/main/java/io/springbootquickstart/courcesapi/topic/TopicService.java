package io.springbootquickstart.courcesapi.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
	private List<Topic> topics = new ArrayList<Topic>(Arrays.asList(
			new Topic("java","topic-java", "java description"),
			new Topic("script","java-script", "java-script description"),
			new Topic(".net","topic-net", ".net description")
			));
	
	@Autowired
	private TopicRepository topicRepository;
	
	public List<Topic> getAllTopics(){
		//return topics;
		List<Topic> topics = new ArrayList<Topic>();
		topicRepository.findAll().forEach(topics:: add);
		return topics;
	}
	
	public Topic getTopic(String id) {
		//return topics.stream().filter(t-> t.getId().equals(id)).findFirst().get();
		return topicRepository.findById(id).get();
	}

	public void addTopic(Topic topic) {
		//topics.add(topic);
		topicRepository.save(topic);
	}
	
	public void updateTopic(Topic topic, String id) {
		/*
		 * for(int index=0 ;index < topics.size() ; index++) { Topic t =
		 * topics.get(index); if(t.getId().equals(id)) { topics.set(index, topic);
		 * return; } }
		 */
		topicRepository.save(topic);
	}
	
	public void deleteTopic(String id) {
		//topics.removeIf(t -> t.getId().equals(id));
		topicRepository.deleteById(id);
	}
}
