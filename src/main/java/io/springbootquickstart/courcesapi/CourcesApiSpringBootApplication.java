package io.springbootquickstart.courcesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourcesApiSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourcesApiSpringBootApplication.class, args);
	}

}
